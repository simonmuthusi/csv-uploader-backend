# -*- coding: utf-8 -*-
# @Author: Simon Muthusi
# @Email: simonmuthusi@gmail.com
# @Date:   2019-02-25 12:02:00
# @Last Modified by:   Simon Muthusi
# @Last Modified time: 2019-02-25 19:02:15
# Project: csv-uploader

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin

from uploader import urls as uploader_urls

admin.site.site_header = "CSV Uploader API"

urlpatterns = [
    url(r'^', admin.site.urls),
    url(r'^', include(uploader_urls, namespace="uploader")),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
