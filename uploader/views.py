# -*- coding: utf-8 -*-
# @Author: Simon Muthusi
# @Email: simonmuthusi@gmail.com
# @Date:   2019-02-25 12:02:15
# @Last Modified by:   Simon Muthusi
# @Last Modified time: 2019-02-25 14:44:28
# Project: csv-uploader

from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

import json

from uploader.models import Uploads


@csrf_exempt
def get_uploads_data(request):
    """
    Handle get uploads data
    """

    template_dict = {}

    uploads = Uploads.objects.filter(is_active=True)

    uploads_list = [{"national_id": u.national_id, "first_name": u.first_name, "last_name": u.last_name, "date_of_birth": "{}".format(
        u.date_of_birth), "postal_address": u.postal_address, "gender": u.gender} for u in uploads]

    template_dict['data'] = uploads_list
    template_dict['status'] = True
    template_dict['message'] = "success"

    return HttpResponse(json.dumps(template_dict), content_type="application/json")


@csrf_exempt
def post_uploads_data(request):
    """
    Handle get uploads data
    """

    body = request.body
    try:
        data = json.loads(body)
    except:
        return HttpResponse(json.dumps({"status": False, "message": "Pass valid JSON"}), content_type="application/json")

    error_list = list()
    for item in data:
        upload = Uploads.objects.filter(national_id=item['national_id'])
        try:
            if upload.count() == 0:
                # do a new insert
                upload = Uploads.objects.create(**item)
            else:
                # update the data
                upload = Uploads.objects.update(**item)
        except Exception as e:
            error_list.append({"item": item, "error": str(e)})

        # get post data
    return HttpResponse(json.dumps(error_list), content_type="application/json")
