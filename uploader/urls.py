# -*- coding: utf-8 -*-
# @Author: Simon Muthusi
# @Email: simonmuthusi@gmail.com
# @Date:   2019-02-25 12:14:57
# @Last Modified by:   Simon Muthusi
# @Last Modified time: 2019-02-25 14:33:34
# Project: csv-uploader

from django.conf.urls import url

from uploader.views import get_uploads_data, post_uploads_data

urlpatterns = [
    url(r'^api/v1/uploads/list/', get_uploads_data),
    url(r'^api/v1/uploads/add/', post_uploads_data),
]
