# -*- coding: utf-8 -*-
# @Author: Simon Muthusi
# @Email: simonmuthusi@gmail.com
# @Date:   2019-02-25 12:02:15
# @Last Modified by:   Simon Muthusi
# @Last Modified time: 2019-02-25 12:12:26
# Project: csv-uploader

from django.contrib import admin

from uploader.models import Uploads


class UploadsAdmin(admin.ModelAdmin):
    list_display = ("national_id", "is_active", )

admin.site.register(Uploads, UploadsAdmin)
