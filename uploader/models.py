# -*- coding: utf-8 -*-
# @Author: Simon Muthusi
# @Email: simonmuthusi@gmail.com
# @Date:   2019-02-25 12:02:15
# @Last Modified by:   Simon Muthusi
# @Last Modified time: 2019-02-25 13:17:48
# Project: csv-uploader

from django.db import models


class Uploads(models.Model):
    '''Uploads data'''
    national_id = models.CharField(
        max_length=50, primary_key=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    date_of_birth = models.DateField()
    postal_address = models.CharField(max_length=100)
    gender = models.CharField(max_length=15)
    is_active = models.BooleanField(default=True)
