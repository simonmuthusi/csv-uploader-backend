# CSV Uploader Service

This is CSV Uploader RESTful service. 
This project is based on python

## Installation
- Clone the app
- Create a virtualenvironment `virtualenv -p python3 env`
- Install requirements `pip install -r requirements.txt`
- Create database and update your `settings.py` file to reflect these changes
- Run the server `python manage.py runserver`

## API structure
### Listing items
GET [base URL]/api/v1/uploads/list/
```{
  "data": [
    {
      "national_id": "12345677",
      "first_name": "John",
      "last_name": "Appleseed",
      "date_of_birth": "1990-10-10",
      "postal_address": "13 Kiambu",
      "gender": "Male"
    },
    {
      "national_id": "12345678",
      "first_name": "Sam",
      "last_name": "Sam",
      "date_of_birth": "2000-01-01",
      "postal_address": "123 Karen",
      "gender": "Male"
    },
    {
      "national_id": "12345679",
      "first_name": "Jane",
      "last_name": "Ode",
      "date_of_birth": "2000-02-02",
      "postal_address": "Westlands",
      "gender": "Female"
    }
  ],
  "status": true,
  "message": "success"
}
```

### Adding new records
POST [base URL]api/v1/uploads/add/
```
[
  {
    "national_id": "12345678",
    "first_name": "sam",
    "last_name": "sam",
    "date_of_birth": "2019-01-01",
    "postal_address": "Postal",
    "gender": "Male"
  }
]
```

## Logins
These are sample logins
URL: https://simon-csv-uploader.herokuapp.com
Username/password: testing/simon2019

## Screenshots
![Main view](screenshots/main.png)
![Listing](screenshots/listing.png)
![Edit](screenshots/edit.png)

